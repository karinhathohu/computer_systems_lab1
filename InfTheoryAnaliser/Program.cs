﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace InfTheoryAnaliser
{
    class Letters {
        public Letters()
        {
            letters = new char[10000];
            counters = new int[10000];
            frequencies = new double[10000]; //pi
            indexFree = 0;
            allSymbolsCount = 0; // m 
        }
        public char[] letters;
        public int[] counters;
        public double[] frequencies;
        public int indexFree;
        public int allSymbolsCount;
    }
    static class Base64Stuff
    {
        static string encodingAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
        static public string Encode(string source)
        {
            byte[] fileBytes = Encoding.UTF8.GetBytes(File.ReadAllText(source));
            List<char> result = new List<char>();
            int i;
            for (i = 0; i < fileBytes.Length / 3; i++) //i * 3 + 0,1,2,3
            {
                result.Add(encodingAlphabet[(fileBytes[3 * i + 0] >> 2)]);
                result.Add(encodingAlphabet[((fileBytes[3 * i + 0] & 3) << 4) + ((fileBytes[3 * i + 1] & 240) >> 4)]);
                result.Add(encodingAlphabet[((fileBytes[3 * i + 1] & 15) << 2) + ((fileBytes[3 * i + 2] & 192) >> 6)]);
                result.Add(encodingAlphabet[fileBytes[3 * i + 2] & 63]);
            }
            if (fileBytes.Length % 3 == 2)
            {
                result.Add(encodingAlphabet[fileBytes[3 * i + 0] >> 2]);
                result.Add(encodingAlphabet[((fileBytes[3 * i + 0] & 3) << 4) + ((fileBytes[3 * i + 1] & 240) >> 4)]);
                result.Add(encodingAlphabet[(fileBytes[3 * i + 1] & 15) << 2]);
                result.Add('=');
            }
            else if (fileBytes.Length % 3 == 1)
            {
                result.Add(encodingAlphabet[fileBytes[3 * i + 0] >> 2]);
                result.Add('=');
                result.Add('=');
            }
            return string.Join("", result.ToArray());
        }
    } 
    class Program
    {
        static void ReadLetterFrequenciesFromFile(string fileName, Letters listOfLetters)
        {
            using (var fileStream = File.OpenRead(fileName))
                using (StreamReader sreader = new StreamReader(fileStream, Encoding.Unicode, true))
                {
                    string line = "";
                    while ((line = sreader.ReadLine()) != null)
                    {
                        foreach (char c in line)
                        {
                            if (listOfLetters.letters.Contains(c))
                            {
                                listOfLetters.counters[Array.FindIndex(listOfLetters.letters, x => x == c)]++;
                            }
                            else
                            {
                                listOfLetters.letters[listOfLetters.indexFree] = c;
                                listOfLetters.counters[listOfLetters.indexFree] = 1;
                                listOfLetters.indexFree++;
                            }
                            listOfLetters.allSymbolsCount++;
                        }
                    }
                }
            for (int i = 0; i < listOfLetters.indexFree; i++)
            {
                listOfLetters.frequencies[i] = (1.0 * listOfLetters.counters[i])  / (1.0 * listOfLetters.allSymbolsCount);
            }
        }
        static void CountEntropy(Letters listOfLetters, ref double entropy)
        {
            entropy = 0;
            for (int i = 0; i < listOfLetters.indexFree ; i++)
            {
                entropy += listOfLetters.frequencies[i] * Math.Log((1.0 / listOfLetters.frequencies[i]),2);
            }
        }
        static void CountInformationQuantity(Letters listOfLetters, ref double infoQuantity, double entropy)
        {
            infoQuantity = entropy * listOfLetters.allSymbolsCount;
        }
        static void ShowResults(double entropy, double infoQuantity, double fileSize)
        {
            Console.WriteLine("{0:F5} {1:F5} {2:F5} {3:F5}", entropy, infoQuantity / 8, fileSize, infoQuantity / fileSize);
        }
        static void ShowFrequencies(Letters listOfLetters)
        {
            for (int i = 0; i < listOfLetters.indexFree; i++)
            {
                Console.WriteLine("{0}   {1}", listOfLetters.letters[i], listOfLetters.frequencies[i]);
            }
        }
        static void Main(string[] args)
        {
            Letters lettersFile1 = new Letters();
            Letters lettersFile2 = new Letters();
            Letters lettersFile3 = new Letters();

            string source1 = "1.txt";
            string source2 = "2.txt";
            string source3 = "3.txt";

            double entropyFile1 = 0;
            double entropyFile2 = 0;
            double entropyFile3 = 0;

            double infoQuantityFile1 = 1;
            double infoQuantityFile2 = 1;
            double infoQuantityFile3 = 1;

            double file1Size = new FileInfo(source1).Length;
            double file2Size = new FileInfo(source2).Length;
            double file3Size = new FileInfo(source3).Length;

            ReadLetterFrequenciesFromFile(source1, lettersFile1);
            CountEntropy(lettersFile1, ref entropyFile1);
            CountInformationQuantity(lettersFile1, ref infoQuantityFile1, entropyFile1);

            ReadLetterFrequenciesFromFile(source2, lettersFile2);
            CountEntropy(lettersFile2, ref entropyFile2);
            CountInformationQuantity(lettersFile2, ref infoQuantityFile2, entropyFile2);

            ReadLetterFrequenciesFromFile(source3, lettersFile3);
            CountEntropy(lettersFile3, ref entropyFile3);
            CountInformationQuantity(lettersFile3, ref infoQuantityFile3, entropyFile3);

            Console.WriteLine("entropy quantity    filesize(bytes)     infoQuantity / fileSize");
            ShowResults(entropyFile1, infoQuantityFile1, file1Size);
            ShowResults(entropyFile2, infoQuantityFile2, file2Size);
            ShowResults(entropyFile3, infoQuantityFile3, file3Size);

            ShowFrequencies(lettersFile1);
            ShowFrequencies(lettersFile2);
            ShowFrequencies(lettersFile3);
            //byte b1 = 239;
            //byte b2 = 193;
            //Console.WriteLine(b1 & 240);
            Console.WriteLine(Base64Stuff.Encode("4.txt"));

            Letters lettersFile1b64 = new Letters();
            Letters lettersFile2b64 = new Letters();
            Letters lettersFile3b64 = new Letters();

            string source1b64 = "1b64.txt";
            string source2b64 = "2b64.txt";
            string source3b64 = "3b64.txt";

            double entropyFile1b64 = 0;
            double entropyFile2b64 = 0;
            double entropyFile3b64 = 0;

            double infoQuantityFile1b64 = 1;
            double infoQuantityFile2b64 = 1;
            double infoQuantityFile3b64 = 1;

            double file1Sizeb64 = new FileInfo(source1b64).Length;
            double file2Sizeb64 = new FileInfo(source2b64).Length;
            double file3Sizeb64 = new FileInfo(source3b64).Length;

            ReadLetterFrequenciesFromFile(source1b64, lettersFile1b64);
            CountEntropy(lettersFile1b64, ref entropyFile1b64);
            CountInformationQuantity(lettersFile1b64, ref infoQuantityFile1b64, entropyFile1b64);

            ReadLetterFrequenciesFromFile(source2b64, lettersFile2b64);
            CountEntropy(lettersFile2b64, ref entropyFile2b64);
            CountInformationQuantity(lettersFile2b64, ref infoQuantityFile2b64, entropyFile2b64);

            ReadLetterFrequenciesFromFile(source3b64, lettersFile3b64);
            CountEntropy(lettersFile3b64, ref entropyFile3b64);
            CountInformationQuantity(lettersFile3b64, ref infoQuantityFile3b64, entropyFile3b64);

            Console.WriteLine("entropy quantity    filesize(bytes)     infoQuantity / fileSize");
            ShowResults(entropyFile1b64, infoQuantityFile1b64, file1Sizeb64);
            ShowResults(entropyFile2b64, infoQuantityFile2b64, file2Sizeb64);
            ShowResults(entropyFile3b64, infoQuantityFile3b64, file3Sizeb64);

            Letters lettersFile1b64bz2 = new Letters();
            Letters lettersFile2b64bz2 = new Letters();
            Letters lettersFile3b64bz2 = new Letters();

            string source1b64bz2 = "1bz2b64.txt";
            string source2b64bz2 = "2bz2b64.txt";
            string source3b64bz2 = "3bz2b64.txt";

            double entropyFile1b64bz2 = 0;
            double entropyFile2b64bz2 = 0;
            double entropyFile3b64bz2 = 0;

            double infoQuantityFile1b64bz2 = 1;
            double infoQuantityFile2b64bz2 = 1;
            double infoQuantityFile3b64bz2 = 1;

            double file1Sizeb64bz2 = new FileInfo(source1b64bz2).Length;
            double file2Sizeb64bz2 = new FileInfo(source2b64bz2).Length;
            double file3Sizeb64bz2 = new FileInfo(source3b64bz2).Length;

            ReadLetterFrequenciesFromFile(source1b64bz2, lettersFile1b64bz2);
            CountEntropy(lettersFile1b64bz2, ref entropyFile1b64bz2);
            CountInformationQuantity(lettersFile1b64bz2, ref infoQuantityFile1b64bz2, entropyFile1b64bz2);

            ReadLetterFrequenciesFromFile(source2b64bz2, lettersFile2b64bz2);
            CountEntropy(lettersFile2b64bz2, ref entropyFile2b64bz2);
            CountInformationQuantity(lettersFile2b64bz2, ref infoQuantityFile2b64bz2, entropyFile2b64bz2);

            ReadLetterFrequenciesFromFile(source3b64bz2, lettersFile3b64bz2);
            CountEntropy(lettersFile3b64bz2, ref entropyFile3b64bz2);
            CountInformationQuantity(lettersFile3b64bz2, ref infoQuantityFile3b64bz2, entropyFile3b64bz2);

            Console.WriteLine("entropy quantity    filesize(bytes)     infoQuantity / fileSize");
            ShowResults(entropyFile1b64bz2, infoQuantityFile1b64bz2, file1Sizeb64bz2);
            ShowResults(entropyFile2b64bz2, infoQuantityFile2b64bz2, file2Sizeb64bz2);
            ShowResults(entropyFile3b64bz2, infoQuantityFile3b64bz2, file3Sizeb64bz2);

            Console.ReadKey();
        }
    }
}
